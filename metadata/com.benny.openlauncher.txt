Categories:System
License:Apache-2.0
Web Site:https://github.com/BennyKok/OpenLauncher/blob/HEAD/README.md
Source Code:https://github.com/BennyKok/OpenLauncher
Issue Tracker:https://github.com/BennyKok/OpenLauncher/issues

Auto Name:OpenLauncher
Summary:Launch applications and manage homescreen
Description:
Launcher that aims to be a powerful and community driven project.
.

Repo Type:git
Repo:https://github.com/BennyKok/OpenLauncher

Build:alpha1-patch1,2
    disable=old build
    commit=32d793ccac3380f022feb9019ee63dc381ff6bf2
    subdir=launcher
    gradle=yes

Build:alpha1-patch2,3
    disable=old build
    commit=c66363c5ed14974d9e416b99ff7106899850ebfc
    subdir=launcher
    gradle=yes

Build:alpha1-patch3,4
    disable=old build
    commit=13324fc4b9f2133cc6271eb78bb52c0ce781f3e5
    subdir=launcher
    gradle=yes

Build:alpha2,5
    disable=old build
    commit=a2180dd217b08ca544375ac0c6e650aafa65855a
    subdir=launcher
    gradle=yes
    prebuild=echo -e "android { lintOptions { disable 'MissingTranslation'\n\ndisable 'ExtraTranslation' } }" >> build.gradle

Build:alpha2_patch1,6
    disable=old build
    commit=00b9280dfe483a1c0ec4a604c6dccc96e29e9fac
    subdir=launcher
    gradle=yes

Build:alpha2_patch2,7
    commit=eb5861ed5688d36dd123b3b48da4af14186a6fa9
    subdir=launcher
    gradle=yes
    prebuild=echo -e "android { lintOptions { disable 'MissingTranslation'\n\ndisable 'ExtraTranslation' } }" >> build.gradle

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:alpha2_patch2
Current Version Code:7
